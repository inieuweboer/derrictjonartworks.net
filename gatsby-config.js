const projectPathPrefix =
  process.env.CI_PROJECT_NAME || "gatsby"

module.exports = {
  // Use CI_PROJECT_NAME variable as pathPrefix, edit/comment if you want to use a custom domain.
  pathPrefix: `/${projectPathPrefix}`,
  siteMetadata: {
    title: `Derric Tjon Artworks`,
    description: `Modern and abstract paintings as well as hand-drawn portraits, from the beautiful country of Suriname.`,
    author: `Ismani Nieuweboer`,
    siteUrl: `https://derrictjonartworks.net`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#d7c078`,
        theme_color: `#d7c078`,
        display: `minimal-ui`,
        icon: `src/images/testlogo.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    `gatsby-plugin-sitemap`,
  ],
}
